<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class createNewUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:new:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates new user according to given parameters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = new User();

        $user->name = $this->ask('Enter the name: ');
        $user->email = $this->ask('Enter the e-mail: ');
        $user->password = Hash::make($this->ask('Enter the password: '));
        $user->save();

        $this->info('Successfully registered user');

        return 0;
    }
}
