<?php

namespace App\Services;

use App\Mail\NewEmployeesEmail;
use App\Models\Employee;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class EmployeeService
{
    public function __construct()
    {
    }

    public function upload(): void
    {

        $employees = $this->getEmployeesFromFile();

        $this->processEmployees($employees);
    }

    public function processEmployees(array $employees): void
    {
        foreach ($employees as $employee) {

            $isValidStartDate = $this->isValidStartDate($employee[5]);

            if ($isValidStartDate) {

                $checkedEmployee = $this->checkCityAndState($employee[4], $employee[3], $employee);

                if (!empty($checkedEmployee)) {

                    $searchEmployee = Employee::where('email', $employee[1])->first();

                    if ($searchEmployee) {


                        $this->updateEmployee($employee, $searchEmployee);
                    } else {

                        $this->createNewEmployee($checkedEmployee);
                    }

                }
            } else {

                throw ValidationException::withMessages(['Error' => 'NOT ACCEPTABLE: Invalid Date'])->status(406);
            }

        }

        $this->sendEmail();

    }

    public function getEmployeesFromFile(): array
    {
        $storagePath = storage_path() . '/app/public/employees/';
        $fileName = "new_employees.csv";
        $openFile = fopen($storagePath . $fileName, 'r');
        $employees = [];
        $row = 0;
        $skip_row_number = ["1"];

        if ($openFile !== FALSE) {

            while (($data = fgetcsv($openFile, 1000, ",")) !== FALSE) {
                $row++;

                if (in_array($row, $skip_row_number)) {
                    continue;
                } else {
                    $employees[] = $data;
                }
            }
        } else {

            throw ValidationException::withMessages(['Error' => 'File not found in ' . '/app/public/employees/'])->status(500);
        }

        fclose($openFile);

        return $employees;
    }

    public function checkCityAndState($state, $city, $employee): array
    {

        $response = Http::get('http://servicodados.ibge.gov.br/api/v1/localidades/estados/' . $state . '/municipios');

        foreach ($response->json() as $district) {

            if ($district["nome"] == $city) {

                $employee[4] = $district['microrregiao']['mesorregiao']['UF']['sigla'];

                return $employee;
            }
        }

        $response = Http::get('http://servicodados.ibge.gov.br/api/v1/localidades/municipios');

        foreach ($response->json() as $district) {

            if ($district["nome"] == $city) {

                $employee[4] = $district['microrregiao']['mesorregiao']['UF']['sigla'];

                return $employee;
            }
        }

        return [];
    }


    public function isValidStartDate(string $startDate): bool
    {
        $format = 'Y-m-d';

        $day = DateTime::createFromFormat($format, $startDate);

        return $day && $day->format($format) === $startDate;
    }

    public function createNewEmployee(array $employee): Employee
    {
        $newEmployee = new Employee();

        $newEmployee->name = $employee[0];
        $newEmployee->email = $employee[1];
        $newEmployee->document = $employee[2];
        $newEmployee->city = $employee[3];
        $newEmployee->state = $employee[4];
        $newEmployee->start_date = $employee[5];
        $newEmployee->user_id = Auth::user()->id;

        $newEmployee->save();

        return $newEmployee;
    }

    public function updateEmployee(array $employee, Employee $oldEmployee): Employee
    {

        $oldEmployee->name = $employee[0];
        $oldEmployee->email = $employee[1];
        $oldEmployee->document = $employee[2];
        $oldEmployee->city = $employee[3];
        $oldEmployee->state = $employee[4];
        $oldEmployee->start_date = $employee[5];
        $oldEmployee->user_id = Auth::user()->id;

        $oldEmployee->save();

        return $oldEmployee;
    }

    public function sendEmail(): void
    {
        Mail::to(Auth::user()->email)->send(new NewEmployeesEmail());
    }
}
