<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = ($request->all());

        $token = auth('api')->attempt($credentials);

        if($token)
        {
            return response()->json(["token" => $token]);
        } 

        return response()->json(["error" => "Invalid credentials"], 403);
        
    }
}
